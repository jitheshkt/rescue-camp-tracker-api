var express = require('express');
var router = express.Router();
var respond = require('./helpers/responder');


var camps = require('./controllers/camps');

router.get('/', (req, res) => {
    respond(res, true, 'API is up and running');
});


router.get('/camps/all', camps.all);
router.post('/camps/new', camps.create);
router.post('/camps/update/:id', camps.update);


module.exports = router;