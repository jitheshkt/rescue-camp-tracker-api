var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CampsSchema = new Schema({
    name: {
        type: String
    },
    address: {
        type: String
    },
    contact_numbers: {
        type: String
    },
    capacity: {
        type: String
    },
    we_want: {
        type: String
    },
    we_have: {
        type: String
    },
    location: {
        lat: String,
        lng: String,
        address: String
    }
}, {
    timestamps: true
});

var Camps = mongoose.model('camps', CampsSchema);

module.exports = Camps;