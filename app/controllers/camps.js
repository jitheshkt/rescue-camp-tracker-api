var CampModel = require('../models/camps');
var respond = require('../helpers/responder');


exports.create = (req, res) => {
    var camp = new CampModel({
        'name': req.body.name,
        'address': req.body.address,
        'contact_numbers': req.body.contact_numbers,
        'capacity': req.body.capacity,
        'we_want': req.body.we_want,
        'we_have': req.body.we_have,
        'location': req.body.location
    });
    camp.save((err) => {
        if (err) {
            console.log(err);
            respond(res, false, 'one or more mapping failed.', [], 406);
        } else {
            respond(res, true, 'camp created successfully', {
                'id': camp._id,
                'name': camp.name
            });
        }
    });
}

exports.all = (req, res) => {
    CampModel.find({}).exec((err, camps) => {
        if(err) {
            console.log(err);
            respond(res, false, 'something went wrong when retreaving camps.', [], 500);
        }
        var campsArray = [];
        for(var key in camps) {
            if(camps.hasOwnProperty(key)) {
                let responseObject = {};
                responseObject.id = camps[key]._id;
                responseObject.name = camps[key].name;
                responseObject.address = camps[key].address;
                responseObject.contact_numbers = camps[key].contact_numbers;
                responseObject.capacity = camps[key].capacity;
                responseObject.we_want = camps[key].we_want;
                responseObject.we_have = camps[key].we_have;
                responseObject.location = camps[key].location;
                responseObject.createdAt = camps[key].createdAt;
                responseObject.updatedAt = camps[key].updatedAt;
                campsArray.push(responseObject);
            }
        }
        respond(res, true, 'all camps', campsArray);
    })
}

exports.update = (req, res) => {
    let updateObject = {};
    if (req.body.name && req.body.name.length > 0) {
        updateObject.name = req.body.name;
    }
    if (req.body.address && req.body.address.length > 0) {
        updateObject.address = req.body.address;
    }
    if (req.body.contact_numbers && req.body.contact_numbers.length > 0) {
        updateObject.contact_numbers = req.body.contact_numbers;
    }
    if (req.body.capacity && req.body.capacity.length > 0) {
        updateObject.capacity = req.body.capacity;
    }
    if (req.body.we_want && req.body.we_want.length > 0) {
        updateObject.we_want = req.body.we_want;
    }
    if (req.body.we_have && req.body.we_have.length > 0) {
        updateObject.we_have = req.body.we_have;
    }
    if (req.body.location && req.body.location.length > 0) {
        updateObject.location = req.body.location;
    }
    CampModel.findOneAndUpdate({
        '_id': req.params.id
    }, {
        $set: updateObject
    }, (error, done) => {
        if (error) {
            if (error.name === 'CastError') {
                respond(res, false, 'relational mapping failed', [], 404);
            } else {
                respond(res, false, 'internal server error', [], 500);
            }
        } else {
            respond(res, true, 'camp updated successfully', []);
        }
    });
}