const sendResponds = (res, status, message = 'operation was success', data = [], code = 200) => {
    return res.status(code).json({
        status: status,
        data: data,
        message: message
    });
}

module.exports = sendResponds;