var express = require('express');
app = express();
var server = require('http').Server(app);
var router = require('./app/routes');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, x-access-token");
  res.header('Access-Control-Allow-Methods', "GET,PUT,POST,DELETE,PATCH,OPTIONS");
  next();
});

app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.json());

const options = {
    keepAlive: 300000,
    connectTimeoutMS: 30000,
    useNewUrlParser: true
};

app.use(morgan('dev'));

mongoose.connect('mongodb://localhost:27017/rescue-camp-api', options).then(() => {
    app.set('appSecret', 'SaveKerala@@!24');
    server.listen(process.env.PORT || 3030);
    console.log("API started and listening at 3030");
}).catch((error) => {
    console.log('error connecting to db: ' + error);
});

app.use('/', router);

module.exports = app;